(ns hello-world-node.core
  (:require [cljs.nodejs :as nodejs]))

(nodejs/enable-util-print!)

(defn -main []
  (println "Hello Wworld!"))

(println "Set Hello!")

(set! *main-cli-fn* -main)
